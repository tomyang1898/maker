package ldh.bean.util;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class BeanInfoUtil {

	public static List<PropertyDescriptor> getFields(Class<?> clazz) {
		List<PropertyDescriptor> pdList = new ArrayList<PropertyDescriptor>();
		try {
			BeanInfo bi = Introspector.getBeanInfo(clazz);
			PropertyDescriptor[] pds = bi.getPropertyDescriptors();
			if (pds != null) {
				for (PropertyDescriptor pd : pds) {
					if (pd.getReadMethod() != null && pd.getWriteMethod() != null) {
						pdList.add(pd);
					}
				}
			}
		} catch (IntrospectionException e) {
			e.printStackTrace();
		}
		return pdList;
	}
	
	public static boolean isSerializable(Class<?> clazz) {
		if (clazz.equals(Serializable.class)) return true;
		Class<?>[] iclazz = clazz.getInterfaces();
		if (iclazz != null) {
			for (Class<?> cz : iclazz) {
				if (cz.equals(Serializable.class)) return true;
			}
		}
		Class<?> pclazz = clazz.getSuperclass();
		if (pclazz != null) {
			return isSerializable(pclazz);
		}
		return false;
	}
	
	public static boolean isWrapClass(Class<?> clz) {   
        try {   
           return ((Class<?>) clz.getField("TYPE").get(null)).isPrimitive();  
        } catch (Exception e) {   
            return false;   
        }   
    }
	
	public static boolean isBaseClass(Class<?> clz) {
		if (clz.isPrimitive()) return true;
        try {   
           return ((Class<?>) clz.getField("TYPE").get(null)).isPrimitive();  
        } catch (Exception e) {   
            return false;   
        }   
    }
	
	public static void main(String[] args) {
		
//		boolean b = isSerializable(Scenic.class);
//		System.out.println(b);
	}
}