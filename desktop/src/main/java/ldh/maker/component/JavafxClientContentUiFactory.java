package ldh.maker.component;

/**
 * Created by ldh on 2017/4/14.
 */
public class JavafxClientContentUiFactory extends ContentUiFactory {

    public ContentUi create() {
        return new JavafxClientContentUi();
    }
}
