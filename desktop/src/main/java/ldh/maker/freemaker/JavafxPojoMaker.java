package ldh.maker.freemaker;

import javafx.beans.property.*;
import javafx.collections.ObservableList;
import ldh.database.Column;

import java.util.List;

public class JavafxPojoMaker extends PojoMaker {

	public JavafxPojoMaker make() {
		data();
		out("JavafxPojo.ftl", data);

		return this;
	}

	public String toString() {
		data();
		return toString("JavafxPojo.ftl", data);
	}

	public void data() {
		for (Column column : table.getColumnList()) {
			if (column.getPropertyClass().isArray()) {
				imports.add(column.getPropertyClass().getComponentType().getName());
			} else {
				imports.add(column.getPropertyClass().getName());
			}

			if (column.isForeign()) {
				this.imports(ObjectProperty.class);
				this.imports(SimpleObjectProperty.class);
			} else if (column.getJavaType().equals("Integer")) {
				this.imports(IntegerProperty.class);
				this.imports(SimpleIntegerProperty.class);
			} else if (column.getJavaType().equals("Long")) {
				this.imports(LongProperty.class);
				this.imports(SimpleLongProperty.class);
			} else if (column.getJavaType().equals("Double")) {
				this.imports(DoubleProperty.class);
				this.imports(SimpleDoubleProperty.class);
			} else if (column.getJavaType().equals("Float")) {
				this.imports(FloatProperty.class);
				this.imports(SimpleFloatProperty.class);
			} else if (column.getJavaType().equals("String")) {
				this.imports(StringProperty.class);
				this.imports(SimpleStringProperty.class);
			} else if (column.getJavaType().equals("Boolean")) {
				this.imports(BooleanProperty.class);
				this.imports(SimpleBooleanProperty.class);
			} else {
				this.imports(ObjectProperty.class);
				this.imports(SimpleObjectProperty.class);
			}
		}

		if (table.getMany() != null && table.getMany().size() > 0) {
			this.imports(ListProperty.class);
			this.imports(SimpleListProperty.class);
			this.imports(ObservableList.class);
		}

		if (table.getManyToManys() != null && table.getManyToManys().size() > 0) {
			this.imports(ListProperty.class);
			this.imports(SimpleListProperty.class);
			this.imports(ObservableList.class);
		}
		super.data();
	}
}
