<?xml version="1.0" encoding="UTF-8"?>

<?import javafx.geometry.*?>
<?import javafx.scene.control.*?>
<?import java.lang.*?>
<?import javafx.scene.layout.*?>

<?import java.net.URL?>
<GridPane styleClass="form" maxHeight="-Infinity" maxWidth="-Infinity" minHeight="-Infinity" minWidth="-Infinity"
          fx:controller="${controllerPackage}.${util.firstLower(table.javaName)}.${table.javaName}EditController" xmlns="http://javafx.com/javafx/8" xmlns:fx="http://javafx.com/fxml/1">
    <children>
        <Label text="${table.text}的信息编辑界面" styleClass="form-title" GridPane.columnSpan="3" GridPane.halignment="LEFT"/>

        <Separator styleClass="form_separator" GridPane.rowIndex="1" GridPane.columnSpan="3"/>

        <#if table.primaryKey.column??>
        <Label text="${util.comment(table.primaryKey.column)}" styleClass="form-label" GridPane.rowIndex="2">
            <tooltip><Tooltip text=""/></tooltip>
        </Label>
        <TextField fx:id="${table.primaryKey.column.property}Text" styleClass="form-pk" editable="false" GridPane.columnIndex="1" GridPane.rowIndex="2" />
        </#if>

        <#list table.columnList as column>
            <#if table.primaryKey.column?? && table.primaryKey.column.property != column.property>
                <#if column.foreign>
        <Label text="${util.comment(column)}" styleClass="form-label" GridPane.rowIndex="${column_index + 2}" />
        <TextField fx:id="${column.property}Text" styleClass="form-text" GridPane.columnIndex="1" GridPane.rowIndex="${column_index + 2}" />
        <Button text="select" GridPane.columnIndex="2" GridPane.rowIndex="${column_index + 2}" style="btn, btn-info"/>
                <#elseif util.isDate(column)>
        <Label text="${util.comment(column)}" styleClass="form-label" GridPane.rowIndex="${column_index + 2}" />
        <DatePicker fx:id="${column.property}DatePicker" styleClass="form-text" GridPane.columnIndex="1" GridPane.rowIndex="${column_index + 2}" GridPane.columnSpan="2"/>
                <#elseif util.isEnum(column)>
        <Label text="${util.comment(column)}" styleClass="form-label" GridPane.rowIndex="${column_index + 2}" />
        <ChoiceBox fx:id="${column.property}ChoiceBox" styleClass="form-text" GridPane.columnIndex="1" GridPane.rowIndex="${column_index + 2}" GridPane.columnSpan="2"/>
                <#else>
        <Label text="${util.comment(column)}" styleClass="form-label" GridPane.rowIndex="${column_index + 2}" />
        <TextField fx:id="${column.property}Text" styleClass="form-text" GridPane.columnIndex="1" GridPane.rowIndex="${column_index + 2}" />
                </#if>
            </#if>

        </#list>
        <HBox styleClass="form_btns" GridPane.rowIndex="${table.columnList?size+2}" GridPane.columnSpan="3" alignment="CENTER_RIGHT">
            <children>
                <Button text="Cancel" fx:id="cancelBtn" onAction="#closeAct" styleClass="btn, btn-cancel" />
                <Button text="Submit" onAction="#submitAct" styleClass="btn, btn-primary" />
            </children>
        </HBox>
    </children>
    <rowConstraints>
        <RowConstraints prefHeight="40.0"  valignment="BOTTOM"/>
        <RowConstraints maxHeight="10.0" minHeight="10.0" prefHeight="10.0" />
        <RowConstraints maxHeight="30.0" minHeight="30.0" prefHeight="30.0" />
        <#list table.columnList as column>
            <#if table.primaryKey.column?? && table.primaryKey.column.property != column.property>
        <RowConstraints maxHeight="30.0" minHeight="30.0" prefHeight="30.0" />
            </#if>
        </#list>
        <RowConstraints maxHeight="60.0" minHeight="60.0" prefHeight="60.0" valignment="BOTTOM"/>
    </rowConstraints>
    <columnConstraints>
        <ColumnConstraints prefWidth="150" halignment="RIGHT"/>
        <ColumnConstraints hgrow="ALWAYS"/>
        <ColumnConstraints prefWidth="50"/>
    </columnConstraints>
    <padding>
        <Insets bottom="6.0" left="16.0" right="10.0" top="20.0" />
    </padding>
    <stylesheets>
        <URL value="@/css/common.css"/>
    </stylesheets>
</GridPane>
