<?xml version="1.0" encoding="UTF-8"?>

<?import javafx.scene.shape.*?>
<?import javafx.scene.canvas.*?>
<?import javafx.geometry.*?>
<?import javafx.scene.control.*?>
<?import java.lang.*?>
<?import javafx.scene.layout.*?>
<?import ${projectPackage}.cell.DateTableCellFactory?>

<?import ldh.fx.component.table.GridTable?>
<?import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView?>
<?import javafx.scene.control.cell.PropertyValueFactory?>
<VBox xmlns="http://javafx.com/javafx/8" xmlns:fx="http://javafx.com/fxml/1" fx:controller="${projectPackage}.controller.${util.firstLower(table.javaName)}.${table.javaName}MainController">
    <children>
        <Label text="${util.comment(table)}" styleClass="title, pane-margin"/>
        <GridTable VBox.vgrow="ALWAYS" fx:id="${util.firstLower(table.javaName)}GridTable">
            <items>
                <Button onAction="#addData">
                    <graphic><FontAwesomeIconView styleClass="add-graphic"/></graphic>
                </Button>
                <Button onAction="#editData">
                    <graphic><FontAwesomeIconView styleClass="edit-graphic"/></graphic>
                </Button>
                <Button onAction="#removeData">
                    <graphic><FontAwesomeIconView styleClass="remove-graphic"/></graphic>
                </Button>
                <Region HBox.hgrow="ALWAYS"></Region>
                <Button onAction="#searchData">
                    <graphic><FontAwesomeIconView styleClass="search-graphic"/></graphic>
                </Button>
            </items>
            <columns>
                <#list table.columnList as column>
                    <#if column.create>
                        <#if util.isDate(column)>
                <TableColumn text="${util.comment(column)}" <#if column.width??> prefWidth="${column.width}"</#if>>
                    <cellValueFactory><PropertyValueFactory property="${column.property}" /></cellValueFactory>
                    <cellFactory><DateTableCellFactory dateFormat="yyyy-MM-dd HH:mm:ss" /></cellFactory>
                </TableColumn>
                        <#else>
                <TableColumn text="${util.comment(column)}" <#if column.width??> prefWidth="${column.width}"</#if>>
                    <cellValueFactory><PropertyValueFactory property="${column.property}" /></cellValueFactory>
                </TableColumn>
                        </#if>
                    </#if>
                </#list>
            </columns>
        </GridTable>
    </children>
</VBox>
