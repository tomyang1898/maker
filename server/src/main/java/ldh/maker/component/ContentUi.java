package ldh.maker.component;

import javafx.geometry.Orientation;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TreeItem;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import ldh.maker.vo.TreeNode;

/**
 * Created by ldh on 2017/4/15.
 */
public class ContentUi extends BorderPane {

    protected TreeItem<TreeNode> treeItem;

    public void setTreeItem(TreeItem<TreeNode> treeItem) {
        this.treeItem = treeItem;
    }
}
