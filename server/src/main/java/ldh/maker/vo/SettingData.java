package ldh.maker.vo;

import com.google.gson.Gson;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by ldh on 2017/3/25.
 */
public class SettingData {

    private StringProperty projectNameProperty = new SimpleStringProperty();
    private StringProperty authorProperty = new SimpleStringProperty();
    private StringProperty xmlPackageProperty = new SimpleStringProperty();
    private StringProperty basePackageProperty = new SimpleStringProperty();
    private StringProperty pojoPackageProperty = new SimpleStringProperty();
    private StringProperty daoPackageProperty = new SimpleStringProperty();
    private StringProperty servicePackageProperty = new SimpleStringProperty();
    private StringProperty controllerPackageProperty = new SimpleStringProperty();
    private Boolean serviceInterface = true;
    private String json;
    private String dbName;
    private Integer treeNodeId;
    private Integer id;


    public String getProjectNameProperty() {
        return projectNameProperty.get();
    }

    public StringProperty projectNamePropertyProperty() {
        return projectNameProperty;
    }

    public void setProjectNameProperty(String projectNameProperty) {
        this.projectNameProperty.set(projectNameProperty);
    }

    public String getAuthorProperty() {
        return authorProperty.get();
    }

    public StringProperty authorPropertyProperty() {
        return authorProperty;
    }

    public void setAuthorProperty(String authorProperty) {
        this.authorProperty.set(authorProperty);
    }

    public String getXmlPackageProperty() {
        return xmlPackageProperty.get();
    }

    public StringProperty xmlPackagePropertyProperty() {
        return xmlPackageProperty;
    }

    public void setXmlPackageProperty(String xmlPackageProperty) {
        this.xmlPackageProperty.set(xmlPackageProperty);
    }

    public String getBasePackageProperty() {
        return basePackageProperty.get();
    }

    public StringProperty basePackagePropertyProperty() {
        return basePackageProperty;
    }

    public void setBasePackageProperty(String basePackageProperty) {
        this.basePackageProperty.set(basePackageProperty);
    }

    public String getPojoPackageProperty() {
        return pojoPackageProperty.get();
    }

    public StringProperty pojoPackagePropertyProperty() {
        return pojoPackageProperty;
    }

    public void setPojoPackageProperty(String pojoPackageProperty) {
        this.pojoPackageProperty.set(pojoPackageProperty);
    }

    public String getDaoPackageProperty() {
        return daoPackageProperty.get();
    }

    public StringProperty daoPackagePropertyProperty() {
        return daoPackageProperty;
    }

    public void setDaoPackageProperty(String daoPackageProperty) {
        this.daoPackageProperty.set(daoPackageProperty);
    }

    public String getServicePackageProperty() {
        return servicePackageProperty.get();
    }

    public StringProperty servicePackagePropertyProperty() {
        return servicePackageProperty;
    }

    public void setServicePackageProperty(String servicePackageProperty) {
        this.servicePackageProperty.set(servicePackageProperty);
    }

    public String getControllerPackageProperty() {
        return controllerPackageProperty.get();
    }

    public StringProperty controllerPackagePropertyProperty() {
        return controllerPackageProperty;
    }

    public void setControllerPackageProperty(String controllerPackageProperty) {
        this.controllerPackageProperty.set(controllerPackageProperty);
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public Integer getTreeNodeId() {
        return treeNodeId;
    }

    public void setTreeNodeId(Integer treeNodeId) {
        this.treeNodeId = treeNodeId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getServiceInterface() {
        return serviceInterface;
    }

    public void setServiceInterface(Boolean serviceInterface) {
        this.serviceInterface = serviceInterface;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public SettingJson getSettingJson() {
        if (!StringUtils.isEmpty(json)) {
            Gson gson = new Gson();
            return gson.fromJson(json, SettingJson.class);
        }
        return null;
    }

    public void setSettingJson(SettingJson settingJson) {
        Gson gson = new Gson();
        json = gson.toJson(settingJson);
    }
}
