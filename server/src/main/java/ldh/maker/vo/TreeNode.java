package ldh.maker.vo;

import ldh.maker.constants.TreeNodeTypeEnum;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ldh on 2017/2/19.
 */
public class TreeNode<T> {

    private Integer id;
    private String text;
    private TreeNodeTypeEnum type;
    private T data;
    private List<TreeNode> children = new ArrayList<>();
    private TreeNode parent;

    public TreeNode(TreeNodeTypeEnum type, String text, TreeNode parent) {
        this.type = type;
        this.text = text;
        this.parent = parent;
        if (parent == null) {
            id = 1;
        }
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public TreeNodeTypeEnum getType() {
        return type;
    }

    public void setType(TreeNodeTypeEnum type) {
        this.type = type;
    }

    public TreeNode getParent() {
        return parent;
    }

    public void setParent(TreeNode parent) {
        this.parent = parent;
    }

    public List<TreeNode> getChildren() {
        return children;
    }

    public void setChildren(List<TreeNode> children) {
        this.children = children;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
